/*
  Blink
  This program reads the status of the IR sesnor and toggles the status of the BUZZER.

  This example code is in the public domain.

  modified April 22nd 2016
  by Debmego Technologies
 */

// digital pin 2 has the IR sensor output attached to it. Give it a name:
int irOp = 7;

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  // make the IR sensor output pin an input:
  pinMode(irOp, INPUT);
  pinMode(13, OUTPUT);
}

// the loop routine runs over and over again forever:
void loop() {
  // read the input pin:
  int buttonState = digitalRead(irOp);
  
  if(buttonState == 1)
  {
    digitalWrite(13, HIGH);
  }
  else
  {
    digitalWrite(13, LOW);
  }
  // print out the state of the button:
  Serial.println(buttonState);
  delay(1);        // delay in between reads for stability
}



