/* Simple test of the functionality of the photo resistor

Connect the photoresistor one leg to pin 0, and pin to +5V
Connect a resistor (around 10k is a good value, higher
values gives higher readings) from pin 0 to GND. .

----------------------------------------------------

           PhotoR     10K
 +5    o---/\/\/--.--/\/\/---o GND
                  |
 Pin 0 o-----------

----------------------------------------------------
Connect PIN7 to relay. The relay toggles when the hand is kept and retrieved

*/



void setup()
{
    Serial.begin(9600);  //Begin serial communcation
    pinMode( 7, OUTPUT );
}

void loop()
{
    Serial.println(analogRead(0)); //Write the value of the photoresistor to the serial monitor.
     delay(1000); //short delay for faster response to light.
     if(analogRead(0)<1000)
     {
        digitalWrite(7, HIGH);    // turn the LED off by making the voltage LOW
     }
     else
     {
        digitalWrite(7, LOW);    // turn the LED off by making the voltage LOW
     }
}
