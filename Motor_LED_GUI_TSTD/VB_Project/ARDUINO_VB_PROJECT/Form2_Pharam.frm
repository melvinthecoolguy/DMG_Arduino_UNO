VERSION 5.00
Object = "{648A5603-2C6E-101B-82B6-000000000014}#1.1#0"; "MSCOMM32.OCX"
Begin VB.Form Form1 
   ClientHeight    =   4332
   ClientLeft      =   108
   ClientTop       =   456
   ClientWidth     =   4788
   LinkTopic       =   "Form1"
   ScaleHeight     =   4332
   ScaleWidth      =   4788
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command12 
      Caption         =   "OFF"
      Height          =   372
      Left            =   2760
      TabIndex        =   14
      Top             =   3360
      Width           =   492
   End
   Begin VB.CommandButton Command11 
      Caption         =   "ON"
      Height          =   372
      Left            =   2040
      TabIndex        =   13
      Top             =   3360
      Width           =   492
   End
   Begin VB.CommandButton Command10 
      Caption         =   "OFF"
      Height          =   372
      Left            =   2760
      TabIndex        =   12
      Top             =   2760
      Width           =   492
   End
   Begin VB.CommandButton Command9 
      Caption         =   "ON"
      Height          =   372
      Left            =   2040
      TabIndex        =   11
      Top             =   2760
      Width           =   492
   End
   Begin VB.CommandButton Command6 
      Caption         =   "OFF"
      Height          =   372
      Left            =   2760
      TabIndex        =   10
      Top             =   2160
      Width           =   492
   End
   Begin VB.CommandButton Command5 
      Caption         =   "ON"
      Height          =   372
      Left            =   2040
      TabIndex        =   9
      Top             =   2160
      Width           =   492
   End
   Begin VB.CommandButton Command4 
      Caption         =   "OFF"
      Height          =   372
      Left            =   2760
      TabIndex        =   8
      Top             =   1560
      Width           =   492
   End
   Begin VB.CommandButton Command3 
      Caption         =   "ON"
      Height          =   372
      Left            =   2040
      TabIndex        =   7
      Top             =   1560
      Width           =   492
   End
   Begin VB.CommandButton Command2 
      Caption         =   "OFF"
      Height          =   372
      Left            =   2760
      TabIndex        =   6
      Top             =   960
      Width           =   492
   End
   Begin VB.CommandButton Command1 
      Caption         =   "ON"
      Height          =   372
      Left            =   2040
      TabIndex        =   5
      Top             =   960
      Width           =   492
   End
   Begin MSCommLib.MSComm MSComm1 
      Left            =   120
      Top             =   3240
      _ExtentX        =   974
      _ExtentY        =   974
      _Version        =   393216
      DTREnable       =   -1  'True
   End
   Begin VB.Label Label5 
      Caption         =   "www.debmego.com"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      Height          =   252
      Left            =   3000
      TabIndex        =   22
      Top             =   3960
      Width           =   1692
   End
   Begin VB.Label Label4 
      Caption         =   "Connect Arduino in COM Port 7"
      Height          =   372
      Left            =   120
      TabIndex        =   21
      Top             =   3960
      Width           =   2172
   End
   Begin VB.Label Label3 
      Caption         =   "Arduino UNO VB Interface"
      BeginProperty Font 
         Name            =   "Calibri"
         Size            =   16.2
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   372
      Left            =   720
      TabIndex        =   20
      Top             =   360
      Width           =   4092
   End
   Begin VB.Label Label2 
      Caption         =   "PIN6"
      Height          =   252
      Index           =   4
      Left            =   3480
      TabIndex        =   19
      Top             =   3360
      Width           =   1092
   End
   Begin VB.Label Label2 
      Caption         =   "PIN5"
      Height          =   252
      Index           =   3
      Left            =   3480
      TabIndex        =   18
      Top             =   2760
      Width           =   1092
   End
   Begin VB.Label Label2 
      Caption         =   "PIN4"
      Height          =   252
      Index           =   2
      Left            =   3480
      TabIndex        =   17
      Top             =   2160
      Width           =   1092
   End
   Begin VB.Label Label2 
      Caption         =   "PIN3"
      Height          =   252
      Index           =   1
      Left            =   3480
      TabIndex        =   16
      Top             =   1560
      Width           =   1092
   End
   Begin VB.Label Label2 
      Caption         =   "PIN2"
      Height          =   252
      Index           =   0
      Left            =   3480
      TabIndex        =   15
      Top             =   1080
      Width           =   1092
   End
   Begin VB.Label Label1 
      Caption         =   "Motor"
      Height          =   252
      Index           =   4
      Left            =   1080
      TabIndex        =   4
      Top             =   3480
      Width           =   732
   End
   Begin VB.Label Label1 
      Caption         =   "LED 4"
      Height          =   252
      Index           =   3
      Left            =   1080
      TabIndex        =   3
      Top             =   2880
      Width           =   732
   End
   Begin VB.Label Label1 
      Caption         =   "LED 3"
      Height          =   252
      Index           =   2
      Left            =   1080
      TabIndex        =   2
      Top             =   2280
      Width           =   732
   End
   Begin VB.Label Label1 
      Caption         =   "LED 2"
      Height          =   252
      Index           =   1
      Left            =   1080
      TabIndex        =   1
      Top             =   1680
      Width           =   732
   End
   Begin VB.Label Label1 
      Caption         =   "LED 1"
      Height          =   252
      Index           =   0
      Left            =   1080
      TabIndex        =   0
      Top             =   1080
      Width           =   732
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub Command1_Click()

MSComm1.Output = "1" 'OFF the LED

End Sub

Private Sub Command10_Click()
MSComm1.Output = "8"
End Sub

Private Sub Command11_Click()
MSComm1.Output = "9"
End Sub

Private Sub Command12_Click()
MSComm1.Output = "a"
End Sub

Private Sub Command2_Click()
MSComm1.Output = "2"
End Sub

Private Sub Command3_Click()
MSComm1.Output = "3"
End Sub

Private Sub Command4_Click()
MSComm1.Output = "4"
End Sub

Private Sub Command5_Click()
MSComm1.Output = "5"
End Sub

Private Sub Command6_Click()
MSComm1.Output = "6"
End Sub

Private Sub Command9_Click()
MSComm1.Output = "7"
End Sub

Private Sub Form_Load()

MSComm1.RThreshold = 3

MSComm1.Settings = "9600,n,8,1"

MSComm1.CommPort = 7 '(Write Port Number on which your Arduino Board is available).

MSComm1.PortOpen = True

MSComm1.DTREnable = False


End Sub




Private Sub Frame1_DragDrop(Source As Control, X As Single, Y As Single)

End Sub


