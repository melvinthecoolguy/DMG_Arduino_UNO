/*
  Temmperature Sensor
  Turns on the motor if the temperature is high and onboard LED shows the program status
  
  press ctrl+shift+m to see the values on the terminal window

  This example code is in the public domain.

  modified April 22nd 2016
  by Debmego Technologies
 */


// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
    pinMode(13, OUTPUT);
    pinMode(7, OUTPUT); // output PIN for the MOTOR

}

// the loop routine runs over and over again forever:
void loop() {
  // read the input on analog pin 0:
  int sensorValue = analogRead(A0);
  // Convert the analog reading (which goes from 0 - 1023) to a voltage (0 - 5V):
  float voltage = sensorValue;
  
  if(voltage<100)
  {
    digitalWrite(7, LOW); //switch OFF the MOTOR if temperature is low
  }
  else
  {
    digitalWrite(7, HIGH); //switch ON the MOTOR if temperature is high
  }
  
  // print out the value you read:
  Serial.println(voltage);
  
  //Program working indicator
  digitalWrite(13, LOW);   // turn the LED on (HIGH is the voltage level)

}
